import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import History from '../App/History';
import Transaksi from '../App/transaksi';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen name="Transaksi" component={Transaksi} />
      <Tab.Screen name="History" component={History} />
    </Tab.Navigator>
  );
};

export default BottomNav;
