import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Image,
  TextInput,
} from 'react-native';
// import icon from './src/Image/download.png'

const History = () => {
  const [result, setResult] = useState([]);
  const [user, setUser] = useState('');

  const dataHistory = async () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = '';

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(result => {
        const val = JSON.parse(result);
        setResult(Object.entries(val));
        // console.log(result);
      })
      .catch(error => console.log('error', error));
  };
  useEffect(() => {
    dataHistory();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View>
        {/* {result?.map((e, index) => {
              const filter = e.filter(item => {
                return (
                  item?.type?.toLowerCase().includes(ketikan?.toLowerCase()) ||
                  item?.sender
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase()) ||
                  item?.target
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase())
                );
              });
            }
          } */}

        <View style={styles.header}>
          <Text style={styles.headerText}>History Pembelian</Text>
        </View>
        <View>
          <TextInput
            placeholder="SEARCH"
            placeholderTextColor={'white'}
            value={user}
            onChangeText={e => {
              setUser(e);
            }}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            {/* ITEM itu dari si data histroy yang di simpan di usestate */}
            {result?.map((e, index) => {
              // lalu data item yang udh di map di jadiin variabel baru bernama datafilter
              // yang dimana dia akan memfilter atau mencari data sesuai  yang di tetapkan
              const dataFilter = e.filter(item => {
                return (
                  item?.type?.toLowerCase().includes(user.toLowerCase()) ||
                  item?.amount
                    ?.toString()
                    .toLowerCase()
                    .includes(user.toLowerCase()) ||
                  item?.sender
                    ?.toString()
                    .toLowerCase()
                    .includes(user.toLowerCase())
                );
              });
              // sesuadah kita membuat fungsi yang akan di filter
              // maka kita membuat tampilan data yang udh di olah atau di filter
              // return (
              //   // di return dengan tampilan dari data fiter yang di map kembali
              //   <View>
              //     {dataFilter.map((Bersih, index) => {
              //       return (
              //         <>
              //           <Text>{Bersih.sender}</Text>
              //           <Text>{Bersih.type}</Text>
              //           <Text>{Bersih.amount}</Text>
              //         </>
              //       );
              //     })}
              //   </View>
              // );
              return (
                <View key={index}>
                  {dataFilter.map((es, index) => {
                    return (
                      <View
                        style={{
                          flexDirection: 'row',
                          borderWidth: 1,
                          margin: 10,
                          borderRadius: 7,
                          padding: 7,
                          backgroundColor: '#ebe8e1',
                          justifyContent: 'space-between',
                          borderColor: '#8a948b',
                          borderBottomWidth: 4,
                          borderRightWidth: 4,
                          shadowColor: '#8a948b',
                          shadowOffset: {width: 0, height: 2},
                          shadowOpacity: 0.9,
                          shadowRadius: 3,
                          elevation: 3,
                        }}>
                        <View>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                // fontFamily :'TitilliumWeb-Black'
                              }}>
                              Pengirim :{' '}
                            </Text>
                            <Text
                              style={
                                {
                                  // fontFamily :'TitilliumWeb-Black'
                                }
                              }>
                              {es.sender}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                // fontFamily :'TitilliumWeb-Black'
                              }}>
                              Penerima :{' '}
                            </Text>
                            <Text
                              style={
                                {
                                  // fontFamily :'TitilliumWeb-Black'
                                }
                              }>
                              {es.target}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                // fontFamily :'TitilliumWeb-Black'
                              }}>
                              Jenis Transaksi :{' '}
                            </Text>
                            <Text
                              style={
                                {
                                  // fontFamily :'TitilliumWeb-Black'
                                }
                              }>
                              {es.type}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                // fontFamily :'TitilliumWeb-Black'
                              }}>
                              Nominal :{' '}
                            </Text>
                            <Text
                              style={
                                {
                                  // fontFamily :'TitilliumWeb-Black'
                                }
                              }>
                              {es.amount}
                            </Text>
                          </View>
                        </View>
                        {/* <Image
                                  source={icon}
                                  style = {{width : 48, height: 48, marginTop : 14}}
                               /> */}
                      </View>
                    );
                  })}
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1b3c2e',
  },
  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    // borderBottomWidth: 1,
    // borderBottomColor: '#ccc',
    backgroundColor: '#8a948b',
    borderRadius: 80,
    marginTop: 10,
    marginBottom: 20,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  content: {
    // flex: 1,
    // padding: 10,
    // shadowColor:'#002080',
    // shadowOffset:{width:2, height:8},
    // shadowOpacity:0.8,
    // shadowRadius:3,
    // elevation:2,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
    borderWidth: 0,
    margin: 10,
    borderRadius: 10,
    padding: 5,
  },
  itemText: {
    fontSize: 16,
  },
});

export default History;
