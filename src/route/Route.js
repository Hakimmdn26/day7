import React from 'react';
// import GrabFood from '../App/GrabFood';
import {createNativeStackNavigator} from '@react-navigation/native-stack'; // untuk membuat screen bisa di navigasi
import History from '../App/History';
import BottomNav from '../Component/BottomNav';
import Transaksi from '../App/transaksi';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="BottomNav" component={BottomNav} />
      {/* <Stack.Screen name="Transaksi" component={Transaksi} /> */}
      {/* <Stack.Screen name="History" component={History} /> */}
      {/* <Stack.Screen name="GrabFood" component={GrabFood} /> */}
    </Stack.Navigator>
  );
}

export default Route;
